import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileWriter;
import java.util.regex.Pattern;

public class TIN_23_2 {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*googlechrome", "http://tinko.rtz.io/");
		selenium.start();
	}

	@Test
	public void testUntitled() throws Exception {
		selenium.open("http://tinko.rtz.io/p-243241.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("link=������� - 1 ���.");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("css=div.block-subtitle > span")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.waitForPageToLoad("20000");
		selenium.click("link=���� �������, ��������������");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("name=osclogin[email_address]")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("id=btn-forgot-password");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("name=osclogin[email_address]")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.type("name=osclogin[email_address]", "artosis21345213@mail.ru");
		selenium.click("id=btn-restore");
		Thread.sleep(5000);
		selenium.click("id=btn-back");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("name=osclogin[password]")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.type("name=osclogin[email_address]", "artosis21345213@mail.ru");
		selenium.type("name=osclogin[password]", "4793546224");
		selenium.click("id=btn-login");
		selenium.waitForPageToLoad("20000");
		selenium.click("link=������ �������");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������ �����������")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		try(FileWriter writer = new FileWriter("C:\\Users\\�������\\Desktop\\Logs.txt",  true))
        {        
            writer.write("TIN-23(����): �� ������� ���� ������ ����� �� �� ������������. "+"\r\n");  
            writer.flush();
        }

	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

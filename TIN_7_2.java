import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileWriter;
import java.util.regex.Pattern;

public class TIN_7_2 {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*googlechrome", "http://vs7.io/");
		selenium.start();
	}

	@Test
	public void test123() throws Exception {
		selenium.open("http://tinko.rtz.io/");
		selenium.waitForPageToLoad("20000");
		selenium.click("link=������ �������");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=mini-login")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.type("id=mini-login", "artosis12345123@mail.ru");
		selenium.type("id=mini-password", "4793546224");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.open("http://tinko.rtz.io/p-244265.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.click("link=������� - 1 ���.");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=PPS-116")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.waitForPageToLoad("20000");
		assertEquals("������2", selenium.getText("id=billing:firstname"));
		assertEquals("������2", selenium.getText("id=billing:lastname"));
		assertEquals("������2", selenium.getText("id=billing:middlename"));
		try(FileWriter writer = new FileWriter("C:\\Users\\�������\\Desktop\\Logs.txt",  true))
        {        
            writer.write("TIN-7(����): ���� ������� �������. ������, ��������� ��� �����������, �������������. "+"\r\n");  
            writer.flush();
        }
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

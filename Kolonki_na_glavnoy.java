import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;

public class Kolonki_na_glavnoy {
	private Selenium selenium;
	public void proverka()
	{
		assertEquals("� ��������", selenium.getText("link=� ��������"));
		assertEquals("�������", selenium.getText("link=�������"));
		assertEquals("��� ������", selenium.getText("link=��� ������"));
		assertEquals("�����-����", selenium.getText("link=�����-����"));
		assertEquals("��������", selenium.getText("link=��������"));
		assertEquals("����������� ���������", selenium.getText("link=����������� ���������"));
		assertEquals("����������", selenium.getText("link=����������"));
		assertEquals("��������", selenium.getText("link=��������"));
		assertEquals("�������� � ������� �������-�������� ������������", selenium.getText("css=li.cat3.dropdown-submenu > a > span"));
		assertEquals("�������� � ������� ��������� �����������", selenium.getText("css=li.cat185.dropdown-submenu > a > span"));
		assertEquals("�������� � ������� �������� � ���������� ��������", selenium.getText("css=li.cat651.dropdown-submenu > a > span"));
		assertEquals("��������", selenium.getText("css=li.cat806.dropdown-submenu > a > span"));
		assertEquals("�������� � ������� ����������, ����������� ����������", selenium.getText("css=li.cat883.dropdown-submenu > a > span"));
		assertEquals("��������� �������", selenium.getText("css=li.cat1898.dropdown-submenu > a > span"));
		assertEquals("�������� �������������", selenium.getText("css=li.cat1081.dropdown-submenu > a > span"));
		assertEquals("���������������� ������������", selenium.getText("css=li.cat1004.dropdown-submenu > a > span"));
		assertEquals("�����, ������ � ���������� ���", selenium.getText("css=li.cat1909.dropdown-submenu > a > span"));
		assertEquals("������� ������������", selenium.getText("css=li.cat1911.dropdown-submenu > a > span"));
		assertEquals("������ � �������", selenium.getText("css=li.cat1936.dropdown-submenu > a > span"));
		assertEquals("��������� � ��������� ���������", selenium.getText("css=li.cat1948.dropdown-submenu > a > span"));
		//assertEquals("������� �����-����", selenium.getText("css=h2.pricelist__title"));
		//assertEquals("���� �����������", selenium.getText("css=h3"));
		assertEquals("��������", selenium.getText("//html[@id='scroll']/body/div/footer/div/div/div[2]/h3"));
		assertEquals("������", selenium.getText("//html[@id='scroll']/body/div/footer/div/div/div[3]/h3"));
		assertEquals("��������� � ������", selenium.getText("//html[@id='scroll']/body/div/footer/div/div/div[4]/h3"));
		assertEquals("������", selenium.getText("link=������"));
		assertEquals("����������", selenium.getText("link=����������"));
		assertEquals("����������� �����������", selenium.getText("link=����������� �����������"));
		assertEquals("��������", selenium.getText("link=��������"));
		assertEquals("������ ������", selenium.getText("link=������ ������"));
		assertEquals("����������� ������", selenium.getText("link=����������� ������"));
		assertEquals("����������� ���������", selenium.getText("xpath=(//a[contains(text(),'����������� ���������')])[2]"));
		assertEquals("���������� �� ������������� �����", selenium.getText("link=���������� �� ������������� �����"));
		assertEquals("��������", selenium.getText("link=��������"));
		assertEquals("��������", selenium.getText("xpath=(//a[contains(text(),'��������')])[2]"));
		assertEquals("����� �����", selenium.getText("link=����� �����"));
		//assertEquals("�������� ����������� ������� ������������", selenium.getText("css=h1.tinko"));
		//assertEquals("�����������", selenium.getText("//html[@id='scroll']/body/div/div/div/div[3]/h2"));
		//assertEquals("��������� ������������ ������������", selenium.getText("css=img.ga-banner"));
		//assertEquals("���������� �����", selenium.getText("css=a[title=\"���������� �����\"] > img.ga-banner"));
		//assertEquals("������� ��������� �������", selenium.getText("css=a[title=\"������� ��������� �������\"] > img.ga-banner"));
		//assertEquals("���������� ������������ �����������", selenium.getText("css=a[title=\"���������� ������������ �����������\"] > img.ga-banner"));
		//assertEquals("������� ������� ������", selenium.getText("css=a[title=\"������� ������� ������\"] > img.ga-banner"));
		//assertEquals("�������� �������", selenium.getText("css=a[title=\"�������� �������\"] > img.ga-banner"));
		//assertEquals("��������", selenium.getText("css=a[title=\"��������\"] > img.ga-banner"));
		//assertEquals("������ ����� ������������", selenium.getText("css=a[title=\"������ ����� ������������\"] > img.ga-banner"));
		//assertEquals("�����", selenium.getText("css=a[title=\"�����\"] > img.ga-banner"));
		//assertEquals("������", selenium.getText("css=a[title=\"������\"] > img.ga-banner"));
		//assertEquals("�����", selenium.getText("css=input.searchbar__btn.btn-search"));
		assertEquals("�������", selenium.getText("link=�������"));
		assertEquals("������ �������", selenium.getText("link=������ �������"));
		//assertEquals("�������� �����", selenium.getText("link=�������� �����"));
		assertEquals("����� �� ���������:", selenium.getText("css=span.hidden-xs.hidden-sm"));
	}

	
	
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*chrome", "http://tinko.rtz.io/");
		selenium.start();
	}
	

	@Test
	public void testUntitled() throws Exception {
		selenium.open("http://tinko.rtz.io/");
		selenium.waitForPageToLoad("20000");
		proverka();
		for (int i=0;i<7; i++)
		{
			switch (i)
			{
			case 0:{selenium.open("http://tinko.rtz.io/about");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			case 1:{selenium.open("http://tinko.rtz.io/news");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			case 2:{selenium.open("http://tinko.rtz.io/howtobuy");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			case 3:{selenium.open("http://tinko.rtz.io/delivery");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			case 4:{selenium.open("http://tinko.rtz.io/consultants");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			case 5:{selenium.open("http://tinko.rtz.io/community");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			case 6:{selenium.open("http://tinko.rtz.io/contactinfo");
			selenium.waitForPageToLoad("20000");
			proverka(); break;}
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileWriter;
import java.util.regex.Pattern;

public class TIN_3_2 {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*googlechrome", "http://vs7.io/");
		selenium.start();
	}

	@Test
	public void test123() throws Exception {

		selenium.open("http://tinko.rtz.io/p-244265.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("link=������� - 1 ���.");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=PPS-116")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=login-method-reg");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=billing:customer_password")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.type("id=billing:firstname", "�������");
		selenium.type("id=billing:lastname", "�������");
		selenium.type("id=billing:telephone", "51381");
		selenium.type("id=billing:middlename", "���������");
		selenium.type("id=billing:email", "alex_moiseev_s@mail.ru");
		selenium.type("id=billing:customer_password", "4793546224");
		selenium.type("id=billing:confirm_password", "4793546224");
		selenium.click("id=billing:legal_entity");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=billing:company")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.type("id=billing:company", "�������");
		selenium.type("id=billing:company_ceo", "������� ������ ����������");
		selenium.type("id=billing:address_legal", "����� �����������, ��� 26");
		selenium.type("id=billing:bank_name", "�������������");
		selenium.type("id=billing:vat_id", "123456789012");
		selenium.type("id=billing:bank_rs", "324243");
		selenium.type("id=billing:bank_bik", "12345");
		selenium.type("id=billing:kpp", "123");
		selenium.type("id=billing:bank_ks", "1234567");
		selenium.type("id=billing:bank_ks", "12345");
		selenium.click("id=s_method_freeshipping_freeshipping");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=billing:street1")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.type("id=billing:street1", "�������������");
		selenium.type("id=billing:city", "����");
		selenium.type("id=billing:postcode", "123456");
		selenium.click("id=onestepcheckout__submit");
		selenium.waitForPageToLoad("20000");
		assertEquals("on", selenium.getValue("id=login-method-reg"));
		assertEquals("on", selenium.getValue("id=billing:legal_entity"));
		assertEquals("on", selenium.getValue("id=s_method_freeshipping_freeshipping"));
		try(FileWriter writer = new FileWriter("C:\\Users\\�������\\Desktop\\Logs.txt",  true))
        {        
            writer.write("TIN-3(����): ���� ������� �������. � ������, ���� ������������ ���� ��� ������������ email, �������� ������ ����������. "+"\r\n");  
            writer.flush();
        }
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

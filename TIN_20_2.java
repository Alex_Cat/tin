import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileWriter;
import java.util.regex.Pattern;

public class TIN_20_2 {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*googlechrome", "http://tinko.rtz.io/");
		selenium.start();
	}

	@Test
	public void testUntitled() throws Exception {
		selenium.open("http://tinko.rtz.io/p-001001.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=qty")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("xpath=(//button[@type='button'])[7]");
		Thread.sleep(5000);
		selenium.click("link=������� - 1 ���.");
		Thread.sleep(5000);
		assertEquals("129 i", selenium.getText("css=div.summary > span.price"));
		try(FileWriter writer = new FileWriter("C:\\Users\\�������\\Desktop\\Logs.txt",  true))
        {        
            writer.write("TIN-20(����): ���� ������� �������. � ������� ����������� ���������� ���������� �������. "+"\r\n");  
            writer.flush();
        }
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

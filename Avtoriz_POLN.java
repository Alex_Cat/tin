import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Pattern;

public class Avtoriz_POLN {
	private Selenium selenium;
	public void login()
	{
		selenium.open("http://tinko.rtz.io");//���� � ������ ������� � ������� � ���������� ������.
		selenium.waitForPageToLoad("20000");
		selenium.click("link=������ �������");
		selenium.type("id=mini-login", "artosis12345123@mail.ru");
		selenium.type("id=mini-password", "4793546224");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
	}
	
	public void create_profiles_fiz()
	{
		selenium.open("http://tinko.rtz.io/customer/address/new/");
		selenium.waitForPageToLoad("20000");
		selenium.type("name=profile_name", "����������");
		selenium.type("id=telephone", "123");
		selenium.type("id=street_1", "�����");
		selenium.type("id=city", "�����");
		selenium.type("id=zip", "111111");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.click("link=������� ����� �������");
		selenium.waitForPageToLoad("20000");
		selenium.type("name=profile_name", "����������");
		selenium.type("id=telephone", "321");
		selenium.type("id=street_1", "�����");
		selenium.type("id=city", "�������");
		selenium.type("id=zip", "222222");
		selenium.click("id=primary_shipping");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[3]/div/div/div[3]/a[2]/i");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=primary_billing");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
	}
	
	public void create_profiles_ur()
	{
		selenium.open("http://tinko.rtz.io/customer/address/new/");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=legal_entity");
		selenium.type("name=profile_name", "����������");
		selenium.type("id=company", "�������");
		selenium.type("id=company_ceo", "����������� �������� ��������");
		selenium.type("id=address_legal", "����������� �����");
		selenium.type("id=bank_name", "�������-����");
		selenium.type("id=vat_id", "123456789012");
		selenium.type("id=bank_rs", "111111");
		selenium.type("id=bank_bik", "111111");
		selenium.type("id=kpp", "111111");
		selenium.type("id=bank_ks", "111111");
		selenium.type("id=telephone", "123");
		selenium.type("id=street_1", "�����");
		selenium.type("id=city", "�����");
		selenium.type("id=zip", "111111");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.click("link=������� ����� �������");
		selenium.waitForPageToLoad("20000");
		selenium.click("css=div.checkbox > label");
		selenium.click("id=legal_entity");
		selenium.type("name=profile_name", "����������");
		selenium.type("id=company", "���");
		selenium.type("id=company_ceo", "���");
		selenium.type("id=address_legal", "����");
		selenium.type("id=bank_name", "����");
		selenium.type("id=vat_id", "2109877654321");
		selenium.type("id=bank_bik", "222222");
		selenium.type("id=kpp", "2222222");
		selenium.type("id=kpp", "222222");
		selenium.type("id=bank_ks", "222222");
		selenium.type("id=bank_rs", "222222");
		selenium.type("id=telephone", "321");
		selenium.type("id=street_1", "�������");
		selenium.type("id=city", "�������");
		selenium.type("id=zip", "222222");
		selenium.click("id=primary_shipping");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[3]/div/div/div[3]/a[2]/i");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=primary_billing");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
	}
	
	public void create_profiles_solo()
	{
		selenium.open("http://tinko.rtz.io/customer/address/new/");
		selenium.waitForPageToLoad("20000");
		selenium.type("name=profile_name", "�����");
		selenium.type("id=telephone", "123");
		selenium.type("id=street_1", "123");
		selenium.type("id=city", "123");
		selenium.type("id=zip", "123");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[2]/div/div/div[3]/a[2]/i");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=primary_billing");
		selenium.click("id=primary_shipping");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
	}
	
	public void cleaning_profile1()
	{
		selenium.click("link=������ �������"); //���������� ������� ��������.
		selenium.click("link=��� �������");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[2]/div/div/div[3]/a/i");
		selenium.click("id=btnYes");
		selenium.waitForPageToLoad("20000"); //������������� ������� ��������.
	}
	public void cleaning_profile2()
	{
		selenium.click("link=������ �������"); //���������� ������� ��������.
		selenium.click("link=��� �������");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[2]/div/div/div[3]/a/i");
		selenium.click("id=btnYes");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[2]/div/div/div[3]/a/i");
		selenium.click("id=btnYes");
		selenium.waitForPageToLoad("20000");//������������� ������� ��������.
	}
	
	public void cleaning_profile3()
	{
		selenium.open("http://tinko.rtz.io/customer/address/");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div/div/div/div[3]/a[2]/i");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=primary_shipping");
		selenium.click("css=button.btn.btn-primary");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div[2]/div/div/div[3]/a/i");
		selenium.click("id=btnYes");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div/div/div/div[3]/a/i");
		selenium.click("id=btnYes");
		selenium.waitForPageToLoad("20000");
	}
	
	public void cleaning_profile4()
	{
		selenium.open("http://tinko.rtz.io/customer/address/");
		selenium.waitForPageToLoad("20000");
		selenium.click("//div[@id='accordion']/div/div/div/div[3]/a/i");
		selenium.click("id=btnYes");
		selenium.waitForPageToLoad("20000");
	}
	
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*chrome", "http://tinko.rtz.io/");
		selenium.start();
	}

	@Test
	public void testUntitled() throws Exception {
		login();//��������� �� ����� tinko.rtz.io.
		int index=0;
		int[][] mass = new int[32][5];
		String a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a15,a16,a17,a18,a19,a20,a21,a22,a23,a24,a25,a26,a27,a28,a29,a30;
		a1="�������";  //�������� �������� �����������
		a2="������� ���� ����������"; //��� ��������� �����������
		a3="�. ������, ��. ������, ��� 21"; //����� �������� �����������
		a4="������� ����"; //�������� ����� �����������
		a5="123456789011"; //��� �����������
		a6="11111"; //����� ���������� ����� � �����
		a7="11111"; //���
		a8="1"; //���
		a9="11111"; //����� ������������������ �����
		a10="����";//��� �����������
		a11="������";//������� �����������
		a12="��������";//�������� �����������
		a13="+11111111111"; //������� �����������
		a15="�������� �������";//�������� �������� ����������
		a16="������� �.�.";//��� ��������� ����������
		a17="��������, ����� ��������, ���� 12";//����� �������� ����������
		a18="���";//�������� ����� ����������
		a19="123456789012";//��� ����������
		a20="22222";//����� ���������� ����� � ����� ����������
		a21="22222";//��� ����������
		a22="2";//��� ����������
		a23="22222";//����� ������������������ ����� ����������
		a24="����";//��� ����������
		a25="������";//������� ����������
		a26="��������";//�������� ����������
		a27="+22222222222";//������� ����������
		a28="����, ��. ����������";//����� ��������
		a29="����";//�����
		a30="222222";//�������� ������
		 for (int t=0;t<32; t++)
		    {
		    	for (int t1=0; t1<5;t1++)
		    	{
		    		mass[t][t1]=0;        //��������� ����� �������.
		    	}
		    }
		 String binary_chislo;
		    int kolvo,b1,b2,b3,b4,b5=0; 
		    for (int chislo=0; chislo<32;chislo++)
		    {
		    	binary_chislo=Integer.toBinaryString(chislo); //��������� ����� � �������� ���.
		    	kolvo=Integer.valueOf(String.valueOf(binary_chislo.length()));//������� ���������� �������� � �����.
		    	char[] chars = new char[kolvo]; //������� ������ ��������
		    	try(FileWriter writer = new FileWriter("C:\\Users\\�������\\Desktop\\help_logi.txt",  false))//���������� � ���� �����.
		        {        
		            String text = (binary_chislo);
		            writer.write(text);
		            writer.flush();
		        }
		    	File f=new File("C:\\Users\\�������\\Desktop\\help_logi.txt");
		    	try(FileReader reader = new FileReader(f))
		    	{
		    	    char[] charss = new char[5];
		    	    for (int t1=0; t1<5;t1++)
			    	{
			    		charss[t1]=0;        //��������� ����� �������.
			    	}
		    	    reader.read(charss);
		    	    for (index=0; index<kolvo;index++)
		            	{
		    	    	mass[chislo][index]=(charss[index]-48);
		    	    	}
		    	    if (chislo>1 && chislo<4){b1=mass[chislo][0];b2=mass[chislo][1];mass[chislo][0]=b2;mass[chislo][1]=b1;}
		    	    if (chislo>3 && chislo<8){b1=mass[chislo][0];b2=mass[chislo][1];b3=mass[chislo][2];mass[chislo][0]=b3;mass[chislo][1]=b2;mass[chislo][2]=b1;}
		    	    if (chislo>7 && chislo<16){b1=mass[chislo][0];b2=mass[chislo][1];b3=mass[chislo][2];b4=mass[chislo][3];mass[chislo][0]=b4;mass[chislo][1]=b3;mass[chislo][2]=b2;mass[chislo][3]=b1;}
		    	    if (chislo>15){b1=mass[chislo][0];b2=mass[chislo][1];b3=mass[chislo][2];b4=mass[chislo][3];b5=mass[chislo][4];mass[chislo][0]=b5;mass[chislo][1]=b4;mass[chislo][2]=b3;mass[chislo][3]=b2;mass[chislo][4]=b1;}
		    	}
		    }//���������� ���������������� �����.
		   
		for (int i=0; i<32; i++) //������ ��������� ���������
			{
				if (((mass[i][1]==0)&&(mass[i][2]==1)) || ((mass[i][3]==1)&&(mass[i][4]==1))|| ((mass[i][3]==0)&&(mass[i][4]==0))) continue;
				selenium.open("http://tinko.rtz.io/p-247997.html");
				selenium.waitForPageToLoad("20000");
				selenium.click("xpath=(//button[@type='button'])[6]");
				for (int second = 0;; second++) {
					if (second >= 60) fail("timeout");
					try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
					Thread.sleep(1000);
				}
				selenium.click("link=������� - 1 ���.");
				selenium.click("xpath=(//button[@type='button'])[4]");
				selenium.waitForPageToLoad("20000");  //����� ��������� � �������� ���������� ������.
				selenium.type("id=billing:firstname", a10);
				selenium.type("id=billing:lastname", a11); 
				selenium.type("id=billing:middlename", a12); 
				selenium.type("id=billing:telephone", a13);
				if (mass[i][0]==1) 
						{
					selenium.click("id=billing:legal_entity");
					for (int second = 0;; second++) {
						if (second >= 60) fail("timeout");
						try { if (selenium.isElementPresent("id=billing:company")) break; } catch (Exception e) {}
						Thread.sleep(1000);
					} //��������� ������� "����������� ����" � �����������
					selenium.type("id=billing:company", a1); 
					selenium.type("id=billing:company_ceo", a2);
					selenium.type("id=billing:address_legal", a3);
					selenium.type("id=billing:bank_name", a4); 
					selenium.type("id=billing:vat_id", a5); 
					selenium.type("id=billing:bank_rs", a6);
					selenium.type("id=billing:bank_bik", a7);
					selenium.type("id=billing:kpp", a8); 
					selenium.type("id=billing:bank_ks", a9);
						}
				if (mass[i][1]==1)
				{
					selenium.click("id=shipping:different_shipping");
					for (int second = 0;; second++) {
						if (second >= 60) fail("timeout");
						try { if (selenium.isElementPresent("id=shipping:firstname")) break; } catch (Exception e) {}
						Thread.sleep(1000);
					}  //��������� ������� "���������� � ���������� �����������"
					selenium.type("id=shipping:firstname", a24); 
					selenium.type("id=shipping:lastname", a25); 
					selenium.type("id=shipping:middlename", a26);
					selenium.type("id=shipping:telephone", a27);
					if (mass[i][2]==1)
					{
						selenium.click("id=shipping:legal_entity");
						for (int second = 0;; second++) {
							if (second >= 60) fail("timeout");
							try { if (selenium.isElementPresent("id=shipping:company")) break; } catch (Exception e) {}
							Thread.sleep(1000);
						} //��������� ������� "����������� ����" � ����������
						selenium.type("id=shipping:company", a15);
						selenium.type("id=shipping:company_ceo", a16);
						selenium.type("id=shipping:address_legal", a17); 
						selenium.type("id=shipping:bank_name", a18);
						selenium.type("id=shipping:vat_id", a19); 
						selenium.type("id=shipping:bank_rs", a20); 
						selenium.type("id=shipping:bank_bik", a21); 
						selenium.type("id=shipping:kpp", a22);
						selenium.type("id=shipping:bank_ks", a23);
					}
				}
				if (mass[i][3]==1)
				{
					selenium.click("id=s_method_storepickup_storepickup"); //��������� ������� �� ���������
				}
				if (mass[i][4]==1)
				{
					selenium.click("id=s_method_freeshipping_freeshipping");
					for (int second = 0;; second++) {
						if (second >= 60) fail("timeout");
						try { if (selenium.isElementPresent("id=shipping:street1")) break; } catch (Exception e) {}
						Thread.sleep(1000);
					}//��������� ������� �� ��������
					selenium.type("id=billing:street1", a28);
					selenium.type("id=billing:city", a29); 
					selenium.type("id=billing:postcode", a30); 
				}
				try(FileWriter writer = new FileWriter("C:\\Users\\�������\\Desktop\\Logs.txt",  true))
		        {        
		            String text = Integer.toString(mass[i][0]);
		            writer.write(text);
		             text = Integer.toString(mass[i][1]);
		            writer.write(text);
		             text = Integer.toString(mass[i][2]);
		            writer.write(text);
		             text = Integer.toString(mass[i][3]);
		            writer.write(text);
		             text = Integer.toString(mass[i][4]);
		            writer.write(text);
		            writer.write("  -  ���������� ���������� ��� ��������������� ������������;"+"\r\n");  
		            writer.flush();
		        }
				selenium.click("id=onestepcheckout__submit");
				selenium.waitForPageToLoad("20000");
				assertEquals("���� ������ �������. ������� �� �������!", selenium.getText("css=h1")); //�������� �� ��, ��� ������ ������� �� ������������.
				if (mass[i][1]==0) cleaning_profile1();//������� ������ ������� �� ��������� � �������� ���������� ��������: ������� ���� �������.
				if (mass[i][1]==1) cleaning_profile2();//������� ������ ������� �� ��������� � �������� ���������� ��������: ������� ��� �������.
			}
	
		create_profiles_fiz();
		selenium.open("http://tinko.rtz.io/p-247997.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.click("link=������� - 1 ���.");
		selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=shipping:different_shipping");
		selenium.click("id=s_method_freeshipping_freeshipping");
		selenium.click("id=onestepcheckout__submit");
		selenium.waitForPageToLoad("20000");
		assertEquals("���� ������ �������. ������� �� �������!", selenium.getText("css=h1"));
		cleaning_profile3();
		
		create_profiles_ur();
		selenium.open("http://tinko.rtz.io/p-247997.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.click("link=������� - 1 ���.");
		selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=shipping:different_shipping");
		selenium.click("id=s_method_freeshipping_freeshipping");
		selenium.click("id=onestepcheckout__submit");
		selenium.waitForPageToLoad("20000");
		assertEquals("���� ������ �������. ������� �� �������!", selenium.getText("css=h1"));
		cleaning_profile3();
		
		create_profiles_solo();
		selenium.open("http://tinko.rtz.io/p-247997.html");
		selenium.waitForPageToLoad("20000");
		selenium.click("xpath=(//button[@type='button'])[6]");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}
		selenium.click("link=������� - 1 ���.");
		selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.waitForPageToLoad("20000");
		selenium.click("id=shipping:different_shipping");
		selenium.click("id=s_method_freeshipping_freeshipping");
		selenium.click("//form[@id='one-step-checkout-form']/div/fieldset/div[2]/label");
		selenium.click("id=onestepcheckout__submit");
		selenium.waitForPageToLoad("20000");
		assertEquals("���� ������ �������. ������� �� �������!", selenium.getText("css=h1"));
		cleaning_profile4();
		
		    create_profiles_solo();
			selenium.open("http://tinko.rtz.io/p-247997.html");
			selenium.waitForPageToLoad("20000");
			selenium.click("xpath=(//button[@type='button'])[6]");
			for (int second = 0;; second++) {
				if (second >= 60) fail("timeout");
				try { if (selenium.isElementPresent("link=������� - 1 ���.")) break; } catch (Exception e) {}
				Thread.sleep(1000);
			}
			selenium.click("link=������� - 1 ���.");
			selenium.click("xpath=(//button[@type='button'])[4]");
			selenium.waitForPageToLoad("20000");
			selenium.click("id=shipping:different_shipping");
			selenium.click("id=s_method_freeshipping_freeshipping");
			selenium.click("//form[@id='one-step-checkout-form']/div/fieldset/div[2]/label");
			selenium.click("id=onestepcheckout__submit");
			selenium.waitForPageToLoad("20000");
			assertEquals("���� ������ �������. ������� �� �������!", selenium.getText("css=h1"));
			cleaning_profile4();
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
